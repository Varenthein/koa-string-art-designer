'use strict'
var __assign = (this && this.__assign) || Object.assign || function (t) {
  for (var s, i = 1, n = arguments.length; i < n; i++) {
    s = arguments[i]
    for (var p in s) {
      if (Object.prototype.hasOwnProperty.call(s, p)) { t[p] = s[p] }
    }
  }
  return t
}
Object.defineProperty(exports, '__esModule', { value: true })
var ChangePositionType;
(function (ChangePositionType) {
  ChangePositionType[ChangePositionType['Start'] = 1] = 'Start'
  ChangePositionType[ChangePositionType['End'] = 2] = 'End'
  ChangePositionType[ChangePositionType['Move'] = 3] = 'Move'
})(ChangePositionType || (ChangePositionType = {}))
function extractHandle (handle) {
  return handle && handle.$el || handle
}
function getPosWithBoundaries (elementRect, boundingRect, left, top, boundingRectMargin) {
  if (boundingRectMargin === void 0) { boundingRectMargin = {} }
  var adjustedPos = { left: left, top: top }
  var height = elementRect.height, width = elementRect.width
  var topRect = top, bottomRect = top + height, leftRect = left, rightRect = left + width
  var marginTop = boundingRectMargin.top || 0, marginBottom = boundingRectMargin.bottom || 0, marginLeft = boundingRectMargin.left || 0, marginRight = boundingRectMargin.right || 0
  var topBoundary = boundingRect.top + marginTop, bottomBoundary = boundingRect.bottom - marginBottom, leftBoundary = boundingRect.left + marginLeft, rightBoundary = boundingRect.right - marginRight
  if (topRect < topBoundary) {
    adjustedPos.top = topBoundary
  } else if (bottomRect > bottomBoundary) {
    adjustedPos.top = bottomBoundary - height
  }
  if (leftRect < leftBoundary) {
    adjustedPos.left = leftBoundary
  } else if (rightRect > rightBoundary) {
    adjustedPos.left = rightBoundary - width
  }
  return adjustedPos
}
exports.Draggable = {
  bind (el, binding) {
    exports.Draggable.update(el, binding)
  },
  update: function (el, binding) {
    if (binding.value && binding.value.stopDragging) {
      return
    }
    var handler = (binding.value && binding.value.handle && extractHandle(binding.value.handle)) || el

    if (binding && binding.value && binding.value.resetInitialPos) {
      initializeState()
    }

    el.removeEventListener('mousedown', el['listener'])
    if (binding.value.drag) handler.addEventListener('mousedown', mouseDown)
    if (binding.value.drag) handler.setAttribute('draggable', 'true')
    if (binding.value.drag) el['listener'] = mouseDown
    initializeState()

    /********** MOUSE MOVE ***********/

    function mouseMove (event) {
      if (binding.value.stopDragging) {
        return false
      }

      var rect = binding.value.boundingElement.getBoundingClientRect()
      var x = event.clientX - rect.left //x position within the element.
      var y = event.clientY - rect.top //y position within the element.

      if (x > binding.value.boundingElement.offsetWidth - 10) x = binding.value.boundingElement.offsetWidth - 10
      if (y > binding.value.boundingElement.offsetHeight - 10) y = binding.value.boundingElement.offsetHeight - 10
      if (x < 0) x = 0
      if (y < 0) y = 0
      setState({ currentDragPosition: { left: x, top: y } })
      updateElementStyle()
    }

    /************** STOP DRAGGING **************/

    function mouseUp (event) {
      binding.value.stopDragging = true
      document.removeEventListener('mousemove', mouseMove)
      document.removeEventListener('mouseup', mouseUp)
      var state = getState()
      binding.value.onDragEnd({ top: state.currentDragPosition.top, left: state.currentDragPosition.left })
    }

    /************* START DRAGGING  **********/

    function mouseDown (event) {
      event.preventDefault()
      if (binding.value.drag === false) {
        return false
      }
      binding.value.stopDragging = false
      document.addEventListener('mousemove', mouseMove)
      document.addEventListener('mouseup', mouseUp)
    }

    /************ UPDATE ELEM **************/

    function updateElementStyle () {
      var state = getState()
      if (!state.currentDragPosition) {
        return
      }
      el.style.position = 'absolute'
      el.style.left = state.currentDragPosition.left + 'px'
      el.style.top = state.currentDragPosition.top + 'px'
    }

    /**************** INIT STATE *****************/

    function initializeState (event) {
      setState({
        currentDragPosition: {},
        drag: false
      })
    }

    /************ UPDATE STATE **************/

    function setState (partialState) {
      var prevState = getState()
      var state = __assign({}, prevState, partialState)
      handler.setAttribute('draggable-state', JSON.stringify(state))
    }

    /************* GET STATE *************/

    function getState () {
      return JSON.parse(handler.getAttribute('draggable-state')) || {}
    }
  }
}
//# sourceMappingURL=draggable.js.map
