/*************** UTILS FUNCTION PACK ******************/

export function randomString (length) {
  let text = ''
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return text
}

export function currentDate (type) {
  let today = new Date()
  let dd = today.getDate()
  let mm = today.getMonth() + 1
  let yyyy = today.getFullYear()

  if (dd < 10) dd = '0' + dd
  if (mm < 10) mm = '0' + mm

  if (type === 'plain') today = dd + '' + mm + '' + yyyy
  else today = dd + '.' + mm + '.' + yyyy
  return today
}

export function formatDateISO (date) {
  let dd = date.getDate()
  let mm = date.getMonth() + 1
  let yyyy = date.getFullYear()

  if (dd < 10) dd = '0' + dd
  if (mm < 10) mm = '0' + mm

  return yyyy + '-' + mm + '-' + dd
}

export function speak ({ ...args }) {
  let text = args.text || 'Error'
  let voice = args.voice || 'Google polski'
  let volume = args.volume || 1

  let msg = new SpeechSynthesisUtterance()

  msg.text = text
  msg.lang = 'pl'
  msg.volume = parseInt(volume)

  msg.voice = speechSynthesis.getVoices().filter(function (v) { return v.name === voice })[0]

  window.speechSynthesis.speak(msg)
}

export function getFirstMonday () {
  let today = new Date()
  let startDate = new Date()
  let day = today.getDay()

  if (day > 1) {
    let difference = 7 - day + 1
    startDate.setDate(today.getDate() + difference)
  } else startDate.setDate(today.getDate())

  return startDate
}

export function copyArray (arr) {
  return JSON.parse(JSON.stringify(arr))
}

export function copyObj (obj) {
  return JSON.parse(JSON.stringify(obj))
}
