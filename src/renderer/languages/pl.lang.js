export default {

  /* MENU */

  newProject: 'Nowy projekt',
  load: 'Wczytaj',
  save: 'Zapisz',
  saveAs: 'Zapisz jako...',
  exit: 'Wyjdź',
  render: 'Renderuj',

  /* NEW PROJECT */

  ChooseSize: 'Wybierz rozmiar',
  ChooseBase: 'Wybierz bazę',
  incorrectForm: 'Niepoprawny formularz',
  incorrectFormDesc: 'Formularz nie jest wypełniony poprawnie. Popraw błędy i spróbuj ponownie.',
  uploadTemplate: 'Załaduj szablon',

  /* image-upload */
  pathIncorrect: 'Ścieżka nieprawidłowa',
  pathIncorrectDesc: 'Ścieżka do pliku jest nieprawidłowa',
  unknownError: 'Nieznany błąd',
  unknownErrorDesc: 'Nieznany błąd. Spróbuj ponownie!',
  fileIncorrect: 'Plik nieprawidłowy',
  fileIncorrectDesc: 'Format pliku jest nieprawidłowy. Akceptowane formaty to: {formats}',
  sizeIncorrect: 'Rozmiar pliku jest za duży',
  sizeIncorrectDesc: 'Maksymalny rozmiar pliku to {size} Mb',
  chooseFile: 'Wybierz plik',

  /* PROMPT */

  noTitle: 'Brak tytułu',
  fileSaved: 'Plik został zapisany',
  fileSaveError: 'Błąd! Nie udało się zapisać pliku...',
  fileLoaded: 'Plik został załadowany',
  fileLoadError: 'Nie udało się załadować pliku...',

  /* base materials */
  baseBlack: 'Czarny',
  baseCherry: 'Wiśnia',
  baseLimbaCoco: 'Limba czek.',
  baseOakAntic: 'Dąb ant.',
  basePineWood: 'Sosna',
  baseOakSonoma: 'Dąb sonoma',
  baseRigoletto: 'Rigoletto',
  baseWenge: 'Wenge',
  baseWhite: 'Biały',

  /* SHARED */

  cancel: 'Anuluj',
  ok: 'Ok',
  name: 'Nazwa',
  width: 'Szerokośc',
  height: 'Wysokość',
  close: 'Zamknij',
  choose: 'Wybierz'

}
