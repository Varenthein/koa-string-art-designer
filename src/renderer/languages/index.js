import Vue from 'vue'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage'

// import languages
import polish from './pl.lang.js'
import english from './en.lang.js'

Vue.use(MLInstaller)

export default new MLCreate({
  initial: 'polish',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('polish').create(polish),
    new MLanguage('english').create(english)
  ]
})
